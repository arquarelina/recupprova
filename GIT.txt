
Git clone: Clona um reposit�rio em um novo diret�rio.
Git add: Adiciona o conte�do do arquivo ao �ndice.
Git commit: Registrar altera��es no reposit�rio.
Git pull: Buscar e integrar com outro reposit�rio ou uma filial local.
Git push: Atualizar refer�ncias remotas junto com objetos associados.
Git remote: Ele lista o nome de cada remoto que voc� especificou.
Git init: Cria um reposit�rio Git vazio ou reinicializa um existente.
Git config: Permite a voc� ler e definir vari�veis de configura��o que controlam todos os aspectos de como o Git parece e opera.